// $ cargo run --package dns-server --example server

use dns_server::{Builder, DnsRecord};
use permit::Permit;
use signal_hook::consts::{SIGHUP, SIGINT, SIGQUIT, SIGTERM};
use signal_hook::iterator::Signals;

fn main() {
    let top_permit = Permit::new();
    let permit = top_permit.new_sub();
    std::thread::spawn(move || {
        Signals::new([SIGHUP, SIGINT, SIGQUIT, SIGTERM])
            .unwrap()
            .forever()
            .next();
        println!("Shutting down.");
        drop(top_permit);
    });
    let (builder, addr) = Builder::new_random_port().unwrap();
    println!("Listening on UDP port {}", addr.port());
    let records = vec![
        DnsRecord::new_a("aaa.example.com", "10.0.0.1").unwrap(),
        DnsRecord::new_a("aaa.example.com", "10.0.0.2").unwrap(),
        DnsRecord::new_aaaa("aaa.example.com", "2606:2800:220:1:248:1893:25c8:1946").unwrap(),
        DnsRecord::new_cname("bbb.example.com", "ccc.example.com").unwrap(),
    ];
    builder.with_permit(permit).serve_static(&records).unwrap();
}
