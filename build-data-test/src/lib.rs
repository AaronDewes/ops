//! build-data-test
//! ===============
//! This is an integration test for
//! [`build-data`](https://crates.io/crates/build-data).
#![forbid(unsafe_code)]
