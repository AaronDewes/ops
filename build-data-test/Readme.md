build-data-test
===============
This is an integration test for
[`build-data`](https://crates.io/crates/build-data).

License: Apache-2.0
